extends Control


signal level1_pressed
signal level2_pressed
signal level3_pressed


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	emit_signal("level1_pressed")


func _on_BtnLvl2_pressed() -> void:
	emit_signal("level2_pressed")


func _on_BtnLvl3_pressed() -> void:
	emit_signal("level3_pressed")
